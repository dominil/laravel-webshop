@extends('layout')
@section('content')
    @foreach ($products as $item)
        <h2>{{ $item->name }}</h2>
        <h5>{{ $item->description }}</h5>
        <button class="btn btn-warning">Remove to cart</button>
    @endforeach

@endsection
